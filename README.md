# SICOM 3A and Master SIGMA program: Statistical/Machine learning course

## News

<!--
For *doctoral or erasmus students*: for the lab sessions, please come to the IMMAC sessions (group `5PMSAST6_2021_S9_BE_G2` with [ADE](https://edt.grenoble-inp.fr/2021-2022/exterieur/))

Due to the time schedule bug on Monday, October 18: for the students that miss Lab6 on Clustering (mostly IMMAC), this  is **rescheduled on Wednesday 27th, 13:30** (see ADE).-->

##### First course session will take place Monday afternoon, September 12 at Minatec Z306 .

<!--
## `News`
- For the interested students, we can find [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks/X_deep_learning) two demo/tutorial notebooks on deep learning for image classification (convolutional neural nets) with the tensorflow 2.X platform and Keras API.
- For students who have to stay at home for health reasons, and *only those who can't attend the face-to-face course*, there is a zoom link (see the [chamilo page](https://chamilo.grenoble-inp.fr/courses/PHELMA5PMSAST6/index.php?) of the course) to participate in
videoconference to the class every monday from 15:45 to 17:45.




##### Lab7 instructions (Tuesday, October 26)
- Lab7 statement on decision trees and random forests is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab7_statement.md)
- Upload **at the end of the session** your lab 7 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=123903) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)

##### Homework for **Tuesday, October 26**
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/8_trees_randomForest_boosting.pdf)) on decision trees and random forests.
- prepare your questions for the course/lab session!

##### Lab6 instructions (Monday, October 18 + *Wednesday, October 27*)
- Lab6 statement on clustering is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab6_statement.md)
- Upload **at the end of the session** your lab 6 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=123902) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)


##### Homework for **Monday, October 18**
- Finish reading/understanding the notebooks you didn't cover in the previous lab session.
- **read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/7_clustering.pdf)) on clustering (unsupervised classification): **read up to kernel K-means on slide 34**
- prepare your questions for the course/lab session!



##### ~~Lab5 instructions~~
- ~~Lab5 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab5_statement.md)~~
- ~~upload **at the end of the session** your lab 5 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=123901) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~



##### ~~Homework for **Monday, October 11**~~
- ~~**read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/6_support_vector_machines.pdf)) on support vector machines:~~
  - ~~in first reading you can skip the slides 10 to 18 (on constrained convex optimization),~~
  - ~~the introduction to random forest (appendix, slides 43 to 47) is also optional~~
- ~~prepare your questions for the course/lab session!~~

##### ~~Lab4 instructions~~
- ~~Lab4 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab4_statement.md)~~
- ~~upload **at the end of the session** your lab 4 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=121243) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~

##### ~~Homework for **Monday, October 4**~~
- ~~**read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/5_linear_models_regularization.pdf)) on lasso and logistic regression: **read up to end of the slides**~~
- ~~prepare your questions for the course/lab session!~~

##### ~~Lab3 instructions~~

- ~~Lab3 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab3_statement.md)~~
- ~~upload **at the end of the session** your lab 3 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=119340) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~


##### ~~Homework for **Friday, October 1**~~

- ~~**read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/5_linear_models_regularization.pdf)) on linear models: **read up to ridge regression slide 23**~~
- ~~prepare your questions for the course/lab session!~~


##### ~~Lab2 instructions~~

- ~~Lab2 statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab2_statement.md)~~
- ~~upload **at the end of the session** your lab 2 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=118376) (pdf file from your editor, or scanned pdf file of a handwritten paper; code, figures or graphics are not required)~~

##### ~~Homework for **Monday, September 27**~~

- ~~**read the lesson** ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/slides/4_discriminant_analysis.pdf) ) on generative models: discriminant analysis + naïve Bayes~~
- ~~prepare your questions for the course/lab session!~~

##### ~~Lab1 instructions~~
- ~~Lab1 is scheduled on Monday 20 (13:30 - Z102 Minatec for IMMAC+doctoral/erasmus students), and 15:45 - Z012 Minatec for EEH students)~~
- ~~Statement is [here](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/labs/lab1_statement.md)~~
- ~~upload your lab 1 *short report* in the [chamilo assigment task](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=117582) (pdf file from your editor, or scanned pdf file of a handwritten paper;
code, figures or graphics are not required)~~

##### ~~Homework before the first lab on **Monday, September 20**~~
- ~~read and run the [introduction notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks/1_introduction/) `N1_Linear_Classification.ipynb` and `N2_Polynomial_Classification_Model_Complexity.ipynb`~~
- ~~answer the questions of the notebook exercises and upload it (pdf file from your editor, or scanned pdf file of a handwritten sheet) under chamilo in the [assignment tool](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=117272) (those and only those who do not yet have an agalan account can send it to me by email):~~
  - ~~only text explanations are required, no need to copy/paste figure or graphics!~~
  - ~~must not exceed half a length of A4 paper~~

##### ~~First course session will take place Monday afternoon, September 13 at Minatec M256 (face-to-face).~~

-->

##### Homework before the first lab on **Monday, September 19**
- read and run the [introduction notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks/1_introduction/) `N1_Linear_Classification.ipynb` and `N2_Polynomial_Classification_Model_Complexity.ipynb`
- answer the questions of the notebook exercises and upload it (pdf file from your editor, or scanned pdf file of a handwritten sheet) under chamilo in the [assignment tool](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=117272) (those and only those who do not yet have an agalan account can send it to me by email):
  - only text explanations are required, no need to copy/paste figure or graphics!
  - must not exceed half a length of A4 paper


## Welcome to the Statistical Learning course!

You will find in this gitlab repository the necessary material for the teaching of _Machine Learning_:

- course materials for the lessons ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/slides))
- examples and exercises for the labs in the form of [Jupyter python notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks) (`.ipynb` files) and/or via online applications,
- quiz: online tool [Socrative](https://b.socrative.com/login/student/), room *MLSICOM*

These resources will be updated as the sessions progress.

### How to use the notebooks?

The examples and exercises will be done under python 3.x through [scikit-learn](https://scikit-learn.org/), and also [tensorflow](https://www.tensorflow.org/). These are two of the most widely used machine learning packages.

The _Jupyter Notebooks_ (`.ipynb` files) are programs containing both cells of code (for us Python) and cells of markdown text for the narrative side. These notebooks are often used to explore and analyze data. Their processing is done with a `jupyter-notebook`, or `juypyter-lab` application, which is accessed through a web browser.

In order to run them you have several possibilities:

1. Download the notebooks to run them on your machine. This requires a Python environment (> 3.3), and the Jupyter notebook and scikit-learn packages. It is recommended to install them via the [anaconda](https://www.anaconda.com/downloads) distribution which will directly install all the necessary dependencies.

**Or**

2. Use the _mybinder_ service ans links to run them interactively and remotely (online): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/master?urlpath=lab/tree/notebooks) (open the link and wait a few seconds for the environment to load).<br>
  **Warning:** Binder is meant for _ephemeral_ interactive coding, meaning that your own modifications/codes/results will be lost when your user session will automatically shut down (basically after 10 minutes of inactivity)

**Or**

3. Use a `jupyterhub` online service:

  - we recommend the UGA's service, [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr), so that you can run your notebooks on the UGA's computation server while saving your modifications and results. Also useful to launch a background computation (connection with your Agalan account; requires uploading your notebooks+data to the server).
  - alternatively you can use an equivalent `jupyterhub` service. For example the one from google, namely [google-colab](https://colab.research.google.com/), which allows you to run/save your notebooks and also to _share the edition to several collaborators_ (requires a google account and upload your notebooks+data in your Drive)

**Note :** You will also find among the notebooks an introduction to Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks%2F0_python_in_a_nutshell)

### Miscellaneous remarks on the materials

- The slides are designed to be self-sufficient (even if the narrative side is often limited by the format).
- In addition to the slides and bibliographical/web references, we generally propose links or videos (at the beginning or end of the slides) specific to the concepts presented. These lists are of course not exhaustive, and you will find throughout the web many resources, often pedagogical. Feel free to do your own research. <!-- and share it on the [Riot room](https://riot.ensimag.fr/#/room/#sicom-ml:ensimag.fr) if you find it useful. -->

<!-- - [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/54301940e4486a8ece22a910c3efa1b2734ed82d?filepath=notebooks) link to run the examples, *except Deep learning ones* too computationally demanding for the JupyterHub server (use the first solution to run these notebooks with your own ressources...) -->
